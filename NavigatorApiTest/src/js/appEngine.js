function displayCoords() {

    console.log("IN FUNC");

    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(successFunc, errorFunc);
    } else {
        console.error("Geo Location is not available on current platform");
    }

}
function successFunc(position) {
    var locationString = "Latitude: " + position.coords.latitude + " longitude:" + position.coords.longitude;
    console.log(locationString);
    document.getElementById("geoLocation").innerHTML = locationString;

}
function errorFunc() {
    console.error("Error getting coords!");
}

function notifyMe() {

    if (!("Notification" in window)) {
        var title = "Some Notification";
        console.info("location :" + window.location.origin);
        var appRef = navigator.mozApps.getSelf();
        appRef.onsuccess = function (evt) {
            icon = appRef.result.manifest.icons["16"];
            console.log("I am: " + icon);
            var notification = navigator.mozNotification.createNotification(
                title, "Some body",'http://icons.iconseeker.com/png/fullsize/black-glossy/get-info-17.png');
            notification.show();
        }
    } else {
        alert("This browser does not support notifications");
    };
}

function openWebPage(){
    var openURL= new MozActivity({
        name:"view",
        data:{
            type:"url",
            url:"http://endava.com"
        }
    });
}
function openImageViewer(){
    var openURL= new MozActivity({
        name:"pick",
        data:{
            type:"image/jpeg"
        }
    });
}

window.onload = function () {
    document.getElementById("get-coordinates").addEventListener("click", displayCoords);
    document.getElementById("notification-button").addEventListener("click", notifyMe);
    document.getElementById("open-url").addEventListener("click",openWebPage);
    document.getElementById("open-image").addEventListener("click",openImageViewer);
}