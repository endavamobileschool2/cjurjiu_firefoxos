var
//element in which the data received by Page#1 from Page#2 will be displayed
    outputDataReceivedFromPage2,
//element in which the data received by Page#2 from Page#1 will be displayed
    outputDataReceivedFromPage1,
    userDateInputField;

function getURLContents() {
    var urlParams = {};
    var match,
        pl = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) {
            return decodeURIComponent(s.replace(pl, " "));
        },
        query = window.location.search.substring(1);
    while (match = search.exec(query))
        urlParams[decode(match[1])] = decode(match[2]);
    return urlParams;
}
/**
 * Code for Page#1 from here
 */
function sendDataToPage2() {
    var hrefWithData = "page2.html?userDate=" + userDateInputField.val();
    console.log("index_page.html@sendDataToPage2. data:"+hrefWithData);
    $("#go-to-page-2").attr("href", hrefWithData);
}
function getDataFromPage2() {
    var urlParams = getURLContents();
//    document.getElementById("#display-data-received-from-page-2").innerHTML=receivedData;
//    outputDataReceivedFromPage2.css("visibility", "visible");
    var receivedData = "User name:" + urlParams["userName"] + ",user age:" + urlParams["userAge"] + ",user profession:" + urlParams["userProfession"];
    console.log("index_page.html@getDataButton. received data:"+receivedData);
    document.getElementById("#display-data-received-from-page-2").style.visibility='visible';
    document.getElementById("#display-data-received-from-page-2").innerHTML=receivedData;
    //outputDataReceivedFromPage2.text(receivedData);
}
//code to be executed only when page_simple_worker is created
$(document).on('pageinit', '#index_page', function () {
    //set listeners for the two buttons, when the page finishes loading
    $("#go-to-page-2").on("click", sendDataToPage2);
    $("#get-data-from-page-2-button").on("click", getDataFromPage2);
    userDateInputField = $("#user_date_input");
    outputDataReceivedFromPage2 = $("#display-data-received-from-page-2");
    console.log("index_page.html@pageinit");
});
$(document).on("pagebeforeshow", "#index_page", function (event) {
    var urlParams = getURLContents();
    var receivedData = "User name:" + urlParams["userName"] + ",user age:" + urlParams["userAge"] + ",user profession:" + urlParams["userProfession"];
    console.log("index_page.html@pagebeforeshow. received data:"+receivedData);
    document.getElementById("display-data-received-from-page-2").style.visibility="visible";
    document.getElementById("display-data-received-from-page-2").innerHTML=receivedData;
});
/**
 * Code for Page#2 from here
 */
function sendDataToPage1() {
    var hrefDataToPage1 = "index.html?userName=" + $("#user-name").val() + "&userAge=" + $("#user-age").val() + "&userProfession=" + $("#user-profession").val();
    console.log("page2@sendDataToPage1. data: " + hrefDataToPage1);
    $("#back-to-home").attr("href", hrefDataToPage1);
//    window.location.assign(hrefDataToPage1);
}
function getDataFromPage1() {
    var urlParams = getURLContents();
    outputDataReceivedFromPage1.css("visibility", "visible");
    outputDataReceivedFromPage1.text(urlParams['userDate']);
}
//code to be executed only when page_json_worker is created
$(document).on('pageinit', '#page2', function () {
    //set listeners for the two buttons, when the page finishes loading
    $("#back-to-home").on("click", sendDataToPage1);
    $("#get-data-from-page-1-button").on("click", getDataFromPage1);
    outputDataReceivedFromPage1 = $("#display-data-from-page1");
    console.log("page2@pageinit")
});
$(document).on("pagebeforeshow", "#page2", function (event) {
    var urlParams = getURLContents();
    outputDataReceivedFromPage1.css("visibility","visible");
    outputDataReceivedFromPage1.text(urlParams['userDate']);
    console.log("page2@pagebeforeshow")
});
