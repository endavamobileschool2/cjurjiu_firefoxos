onmessage = function (event) {
    if (event.data instanceof Object && event.data.hasOwnProperty("bigArray") && event.data.hasOwnProperty("myMessage")) {
            var reply = "Received the message \"" + event.data.myMessage + "\" and the array! Array[0]=" + event.data.bigArray[1];
            postMessage({
                "msg_from_transferable_worker": reply,
                //obtain the time when the worker sends the reply. this is representative for the when the worker finished processing the large array
                "time_of_response": new Date().getTime()
            });
    }
}