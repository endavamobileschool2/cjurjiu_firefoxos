var
//worker that does nothing besides simulating some background work, and then sends back a string reply
    myWorkerSimple,
//worker that receives a json object, simulates some background work, and then sends back a different json as a reply
    myWorkerJSON,
//worker that does demonstrates the usage and behavior of the importScripts(...) function.
    myWorkerImports,
//worker that demonstrates the advantages of using transferable objects as the data model passed to workers.
// **does not work on firefox os 1.1, must work on older versions**. Can be verified using Firefox for Desktop or Google Chrome
    myWorkerTransferable,
//element in which the output of the simple worker will be displayed
    outputJSONWorker,
//element in which the output of the json worker will be displayed
    outputSimpleWorker,
//element in which the output of the imports worker will be displayed
    outputImportsWorker,
//element in which the output of the transferable objects worker will be displayed
    outputTransferableObjWorker,
// input fields containing the defining coefficients for a second order polynomial(ax^2+bx+c)
    valueOfAInputfield, valueOfBInputfield, valueOfCInputfield,
//will contain a 50MB array, to demonstrate the advantages of transferable objects
    largeArray,
//a variable containig the time at which the user has sent the largeArray to the transferable objects worker. Will be used to compute the delta time, to ilustrate the differences between using the
//transferable interface, and from sending a clone of an object
    initialTime;

/**
 * Code for the button that simulates some work in the main thread from here
 */
function doWorkOnUiThread() {
    //do some work ( should block mai ui thread)
    for (var i = 0; i < 2500000000; i++) {
    }
    //then notify the user
    $("#display-worker-reply").css("visibility", "visible");
    $("#display-worker-reply").text("Hello! This work was done on the main thread");
}
//code to be executed only when index_page is created
$(document).on('pageinit', '#index_page', function () {
    //jquery specific method of adding a listener. this will represent the -do-work-on-ui-thread- button.
    $("#do-work-on-ui-thread").on("click", doWorkOnUiThread);
});

/**
 * Code for Worker that uses only a string for data communication from here
 */
function initializeWorkerSimple() {
    if (typeof myWorkerSimple == 'undefined') {
        //if worker does not exist create the worker
        myWorkerSimple = new Worker('../js/simple_worker.js');
        //set up a handler, which reacts to messages sent by the worker, to the main threads
        myWorkerSimple.onmessage = function (someEvent) {
            //print message in console
            console.log(' Received from worker: "' + someEvent.data + '" ');
            //set the text of the output field, and make it visible, as it's initially in a hidden state
            outputSimpleWorker.css("visibility", "visible");
            outputSimpleWorker.text(' Received from worker: "' + someEvent.data + '" ');
        };
    }
}
function sendDataToWorker() {
    initializeWorkerSimple();
    //send the worker the name inserted by the user, in the text field
    myWorkerSimple.postMessage($("#user-name-simple-worker").val());
}
function killSimpleWorker() {
    //kill the worker
    myWorkerSimple.terminate();
    console.log("Worker was killed");
    outputSimpleWorker.css("visibility", "visible");
    outputSimpleWorker.text(' Worker killed! Ctrl+R to restart the app! ');
}
//code to be executed only when page_simple_worker is created
$(document).on('pageinit', '#page_simple_worker', function () {
    //set listeners for the two buttons, when the page finishes loading
    document.getElementById("send-message-to-worker").addEventListener("click", sendDataToWorker);
    document.getElementById("kill-worker").addEventListener("click", killSimpleWorker);
    outputSimpleWorker = $("#display-simple-worker-reply");
});

/**
 * Code for Worker using JSON Data Communication from here
 */
function initializeWorkerWithJSON() {
    if (typeof myWorkerJSON == 'undefined') {
        //if worker does not exist create the worker
        myWorkerJSON = new Worker('../js/json_worker.js');
        //set up a handler, which reacts to messages sent by the worker, to the main threads
        myWorkerJSON.onmessage = function (someEvent) {
            if (someEvent.data instanceof Object && someEvent.data.hasOwnProperty("greeting") && someEvent.data.hasOwnProperty("reply_to_age") && someEvent.data.hasOwnProperty("reply_to_profession")) {
                //print message in console
                console.log(' Received from worker: "' + someEvent.data.greeting + " " + someEvent.data.reply_to_age + someEvent.data.reply_to_profession + " " + '" ');
                //set the text of the output field, and make it visible, as it's initially in a hidden state
                outputJSONWorker.css("visibility", "visible");
                outputJSONWorker.text(' Received from worker: "' + someEvent.data.greeting + " " + someEvent.data.reply_to_age + someEvent.data.reply_to_profession + " " + '" ');
            } else {
                outputJSONWorker.css("visibility", "visible");
                outputJSONWorker.text(' Invalid object back ');
            }
        };
    }
}
function sendJSONToWorker() {
    initializeWorkerWithJSON();
    //create the json object to send
    var json_message = {

        "name": $("#user-json-name").val(),
        "profession": $("#user-profession").val(),
        "age": $("#user-age").val()
    }
    //send the worker the name inserted by the user, in the text field
    myWorkerJSON.postMessage(json_message);
}
function killJSONWorker() {
    //kill the worker
    myWorkerJSON.terminate();
    console.log("Worker JSON was killed");
    outputJSONWorker.css("visibility", "visible");
    outputJSONWorker.text(' Worker killed! Ctrl+R to restart the app! ');
}
//code to be executed only when page_json_worker is created
$(document).on('pageinit', '#page_json_worker', function () {
    //set listeners for the two buttons, when the page finishes loading
    document.getElementById("send-message-to-json-worker").addEventListener("click", sendJSONToWorker);
    document.getElementById("kill-json-worker").addEventListener("click", killJSONWorker);
    outputJSONWorker = $("#display-json-worker-reply");
});

/**
 * Code for Worker with Imports from here
 */
function initializeWorkerWithImports() {
    if (typeof myWorkerImports == 'undefined') {
        myWorkerImports = new Worker("../js/worker_with_imports.js");
        myWorkerImports.postMessage("Initialize");
        myWorkerImports.onmessage = function (someEvent) {
            if (someEvent.data instanceof Object && someEvent.data.hasOwnProperty("x1") && someEvent.data.hasOwnProperty("x2")) {
                //print message in console
                console.log(' Received from worker: "' + someEvent.data.x1 + " " + someEvent.data.x2 + ' " ');
                //set the text of the output field, and make it visible, as it's initially in a hidden state
                outputImportsWorker.css("visibility", "visible");
                outputImportsWorker.text(' Received from worker: "' + someEvent.data.x1 + " " + someEvent.data.x2 + '" ');
            } else {
                outputImportsWorker.css("visibility", "visible");
                outputImportsWorker.text(' Received from worker: "' + someEvent.data + '" ');
            }
        };
    }
}
function sendDataToImportsWorker() {

    initializeWorkerWithImports();

    var a = valueOfAInputfield.val();
    var b = valueOfBInputfield.val();
    var c = valueOfCInputfield.val();

    if (a && b && c) {
        myWorkerImports.postMessage({
            "a": valueOfAInputfield.val(),
            "b": valueOfBInputfield.val(),
            "c": valueOfCInputfield.val()
        });
    } else {
        outputImportsWorker.css("visibility", "visible");
        outputImportsWorker.text(' Empty fields are not allowed, please try again. ');
    }
}
function getMessageFromScript1() {
    initializeWorkerWithImports();
    myWorkerImports.postMessage("script1");
}
function getMessageFromScript2() {
    initializeWorkerWithImports();
    myWorkerImports.postMessage("script2");
}
function killImportsWorker() {
    //kill the worker
    myWorkerImports.terminate();
    console.log("Worker Imports was killed");
    outputImportsWorker.css("visibility", "visible");
    outputImportsWorker.text(' Worker killed! Ctrl+R to restart the app! ');
}
//code to be executed only when page_imports_worker is created
$(document).on('pageinit', '#page_imports_worker', function () {
    //set listeners for the two buttons, when the page finishes loading
    outputImportsWorker = $("#display-imports-worker-reply");
    valueOfAInputfield = $("#value_of_a");
    valueOfBInputfield = $("#value_of_b");
    valueOfCInputfield = $("#value_of_c");
    document.getElementById("send-data-to-imports-worker").addEventListener("click", sendDataToImportsWorker);
    document.getElementById("kill-imports-worker").addEventListener("click", killImportsWorker);
    $("#get-message-from-script1").on("click", getMessageFromScript1);
    $("#get-message-from-script2").on("click", getMessageFromScript2);
});

/**
 * Transferable Objects code to follow from here
 */
function createLargeArray() {
    largeArray = new Uint8Array(1024 * 1024 * 50); // 32MB
    for (var i = 0; i < largeArray.length; ++i) {
        largeArray[i] = i;
    }
}
function initializeWorkerWithTransferableObjs() {
    if (typeof myWorkerTransferable == 'undefined') {
        myWorkerTransferable = new Worker("../js/worker_transferable_objects.js");
        myWorkerTransferable.postMessage("initialize");
        myWorkerTransferable.onmessage = function (someEvent) {
            if (someEvent.data instanceof Object && someEvent.data.hasOwnProperty("msg_from_transferable_worker") && someEvent.data.hasOwnProperty("time_of_response")) {
                outputTransferableObjWorker.css("visibility", "visible");
                var text_from_worker = "Main thread received from worker: \"" + someEvent.data.msg_from_transferable_worker + "\"";
                var time_delta = someEvent.data.time_of_response - initialTime;
                var outputText = text_from_worker + " Round trip tme:" + time_delta;
                outputTransferableObjWorker.text(outputText);
            } else {
                outputTransferableObjWorker.css("visibility", "visible");
                outputTransferableObjWorker.text("Invalid reply from transferable worker!");
            }
        };
    }
}
function sendDataToTransferableByCopy() {
    initializeWorkerWithTransferableObjs();
    var messageToSend = {
        "myMessage": "Data passed by copy",
        "bigArray": largeArray
    }
    initialTime = new Date().getTime();
    myWorkerTransferable.postMessage(messageToSend);
}
function sendDataToTransferableByTransfer() {
    initializeWorkerWithTransferableObjs();
    var messageToSend = {
        "myMessage": "Data passed with transferable",
        "bigArray": largeArray
    }
    initialTime = new Date().getTime();
    myWorkerTransferable.postMessage(messageToSend, [messageToSend.bigArray.buffer]);
}
//code to be executed only when page_imports_worker is created
$(document).on('pageinit', '#page_transferable_object', function () {

    largeArray = new Uint8Array(1024 * 1024 * 50); // 32MB
    for (var i = 0; i < largeArray.length; ++i) {
        largeArray[i] = i;
    }
    //set listeners for the two buttons, when the page finishes loading
    outputTransferableObjWorker = $("#display-transferable-objects-worker-reply");
    document.getElementById("send-array-by-copy").addEventListener("click", sendDataToTransferableByCopy);
    document.getElementById("send-array-by-transferable-object").addEventListener("click", sendDataToTransferableByTransfer);
    document.getElementById("create-large-array").addEventListener("click", createLargeArray);
});