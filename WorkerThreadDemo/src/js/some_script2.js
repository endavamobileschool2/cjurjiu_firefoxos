var some_var_from_script2 = "Var from script2";
function function_from_script2() {
    postMessage("Message from script 2!");
}
function eq_deg2(a, b, c) {
    var delta = compute_delta(a, b, c);
    if (delta < 0) {
        return "Delta must be at least 0!";
    } else {
        var x1, x2;
        x1 = (-b + Math.sqrt(delta)) / (2 * a);
        x2 = (-b - Math.sqrt(delta)) / (2 * a);
        return {
            "x1": x1,
            "x2": x2
        }
    }
}