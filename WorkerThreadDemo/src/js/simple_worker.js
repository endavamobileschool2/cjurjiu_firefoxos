onmessage = function (oEvent) {
    //some time consuming task
    for (var i = 0; i < 2500000000; i++) {
    }
    //reply after it is done. oEvent is the event message sent to the worker.
    // Tthe data sent is in the .data field. In our case, it represents the name of the user.
    postMessage("Hello " + oEvent.data);
};