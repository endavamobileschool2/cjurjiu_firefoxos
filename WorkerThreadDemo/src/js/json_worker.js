onmessage = function (oEvent) {
    //some time consuming task
    for (var i = 0; i < 2500000000; i++) {
    }
    if (oEvent.data instanceof Object && oEvent.data.hasOwnProperty("name") && oEvent.data.hasOwnProperty("age") && oEvent.data.hasOwnProperty("profession")) {
        //verify to see if the object received (oEvent.data) is a json, and if it has the fields we expect require.
        //create another JSON, based on the data received.
        var reply = {
            "greeting": "Hello "+oEvent.data.name+"!",
            "reply_to_age": "When you're " + oEvent.data.age+" years old,",
            "reply_to_profession": "it's a great time to be a/an "+oEvent.data.profession+"."
        }
        //send the new json object, as a reply to the initial message
        postMessage(reply);
    } else {
        postMessage("Invalid object received!");
    }
};