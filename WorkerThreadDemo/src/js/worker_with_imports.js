//handle a message received
onmessage = function (event) {

    //some time consuming task
    for (var i = 0; i < 2500000000; i++) {
    }

    if (event.data instanceof Object && event.data.hasOwnProperty("a") && event.data.hasOwnProperty("b") && event.data.hasOwnProperty("c")) {
        //check to see if the event contains a json with the 3 parameters required to compute the roots of a 2nd order polynomial. if yes, compute the polynomial, using 2 functions:
        //eq_deg2(var,var,var) - declared in the second script file. this function uses another function, compute_delta(var,var,var) in order to obtain the delta value.compute_delta is located in the first script file.
        //this demonstrates that not only structures present in the file calling importScripts(a,b,...,x) (where a,b,..x are file identifiers) can access structures from the imported files, but that elements from file 'a'
        // can also access and use elements from file 'b'.
        var result = eq_deg2(event.data.a, event.data.b, event.data.c);
        postMessage(result);
    } else if (event.data === "Initialize") {
        //if the message is to init the worker, we read the scripts, as interpreting them not done asynchronously
        importScripts("some_script1.js", "some_script2.js");
    } else if (event.data === "script1") {
        //display a sample method form the first script imported. also, display the value of a global variable declared in that script file, to highlight how imported scripts behave
        function_from_script1();
        postMessage("From worker_with_imports.js  var_from_script1 is:" + some_var_from_script1);
    } else if (event.data === "script2") {
        //display a sample method form the second script imported. also, display the value of a global variable declared in that script file, to highlight how imported scripts behave
        function_from_script2();
        postMessage("From worker_with_imports.js, var_from_script2 is:" + some_var_from_script2);
    } else {
        postMessage("Random message from the worker with imports!");
    }
}
